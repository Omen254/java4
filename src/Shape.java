public interface Shape {
    String getName(); //Здесь я объявил метод getName() который будет возвращать мне значение типа стринг
    double getSquare();
    String getColor();
}
