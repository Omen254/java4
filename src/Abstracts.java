import java.util.ArrayList;

public class Abstracts {
    public static void main(String[] args) {// точка входа в программу
        ArrayList<Shape> shapes = new ArrayList<>();
        Shape circle = new Circle(3.4, "Красный");
        Shape rectangle = new Rectangle(4, 6, "Зелёный");
        shapes.add(circle);
        shapes.add(rectangle);
        printShapes(shapes);
    }
        public static void printShapes(ArrayList<Shape>shapes){
            for (Shape shape : shapes) {
                System.out.println("Name: " + shape.getName());
                System.out.println("Square: " + shape.getSquare());
                System.out.println("Color: " + shape.getColor());
            }
        }
}
