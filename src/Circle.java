public class Circle implements Shape {
    private double radius;//Здесь мы объявили переменную типа double(real) с модификатором доступа,область видимости у которого только текущий класс Circle.
    private String color;

    public Circle(double radius, String color) {
        this.radius = radius;
        this.color = color;
    }

    @Override//Переопределение метода который у нас есть в интерфейсе Shape.В интерфейсе Shape все методы абстрактные то есть мы знаем что этот метод будет называться getName и будет возвращать нам обхект типа стринг.А какой конкретно будет наша стринга мы напишем в классе Circle
    public String getName() {
        return "Circle";
    }

    @Override
    public double getSquare() {
        return 3.1415926 * radius * radius ;
    }

    @Override
    public String getColor() {
        return color;
    }
}
