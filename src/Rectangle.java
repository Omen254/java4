public class Rectangle implements Shape {
    private double h;//Объявил переменную h которая обозн. высоту
    private double w;//Объявил переменную w которая обозн. ширину нашего прямоуг.
    private String color;

    public Rectangle(double h, double w, String color) {
        this.h = h;
        this.w = w;
        this.color = color;
    }

    @Override
    public String getName() {
        return "Rectangle";
    }

    @Override
    public double getSquare() {
        return h * w;
    }

    @Override
    public String getColor() {
        return color;
    }
}
